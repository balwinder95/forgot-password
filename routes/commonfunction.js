var sendResponse = require('./sendResponse');
var nodemailer = require('nodemailer');
var emailSettings = require('../config/localDevelopment').emailSettings;

exports.checkBlank = function (arr) {
  return (checkBlank(arr));
};

function checkBlank(arr) {
  var arrlength = arr.length;
  for (var i = 0; i < arrlength; i++) {
    if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
      return 1;
    }
  }
  return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

  var checkBlankData = checkBlank(manValues);

  if (checkBlankData) {
    sendResponse.parameterMissingError(res);
  }
  else {
    callback(null);
  }
}

exports.sendEmail = function(receiver, message, subject, callback) {

  var smtpTransport = nodemailer.createTransport('SMTP', {
    service: 'Gmail',
    auth: {
      user: emailSettings.email,
      pass: emailSettings.password
    }
  });

  var mailOptions = {
    from: 'XYZ <' + emailSettings.email + '>',
    to: receiver,
    subject: subject,
    html: message
  }

  smtpTransport.sendMail(mailOptions, function (error, response) {
  if (error) {
    return callback(0);
  } else {
    return callback(1);
  }
  });
}

