var express = require('express');
var router = express.Router();
var userLogin = require('./userLogin');
var commonfunction = require('./commonfunction');
var async = require('async');

/* GET test page. */
router.get('/test', function (req, res) {
  res.render('test');
});
router.post('/forgotPassword', userLogin.forgotPassword);

router.get('/reset_password', function (req, res) {
  res.render('reset_password');
});
router.post('/resetPassword', userLogin.resetPassword);

module.exports = router;