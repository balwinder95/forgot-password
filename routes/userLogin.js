var commonfunction = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');
var url = require('url');
var port = require('../config/localDevelopment').PORT;

exports.forgotPassword = function (req, res) {

	var email = req.body.email;
	var manValues = [email];

	async.waterfall([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			getUserIdFromEmail(res, email, callback);
		}], function (err, userID) {

			var token = generatePassword(20, false);
			var date = new Date();
			token = md5(date + token);

			var sql = 'UPDATE `user_info` SET `one_time_password_token`=?, `forgot_password_set`=? WHERE `user_id`=? LIMIT 1';
			dbConnection.Query(res, sql, [token, 1, userID], function (response) {

				forgotPasswordRequest(email, token, function (result) {

					if (result == 0) {
						sendResponse.somethingWentWrongError(res);
					} else {
						var data = {"message": "Check your email to change your password."}
            sendResponse.sendSuccessData(data, res);
					}
				});
			});

		});
};


function forgotPasswordRequest (email, token, callback) {

 	var to = email;
  var sub = "Please reset your password";
  var msg = "Hi, <br><br>";
  msg += "http://localhost:" + port + "/resetPassword?token=" + token;

  commonfunction.sendEmail(to, msg, sub, function(result) {
      return callback(result);
  });
}

exports.resetPassword = function (req, res) {

	var link = req.body.link;
	var newPass = req.body.new_password;
	var confirmNewPass = req.body.confirm_new_password;

	var manValues = [link, newPass, confirmNewPass];

	async.waterfall([
		function (callback) {
			commonfunction.checkBlankWithCallback(res, manValues, callback);
		}, function (callback) {
			checkPassAndConfirmPass(res, newPass, confirmNewPass, callback);
		}, function (callback) {
			checkToken(res, link, callback);
		}], function (err, userID) {

			newPassEncrypt = md5(newPass);
			var sql = 'UPDATE `user_info` SET `password`=?, `one_time_password_token`=?, `forgot_password_set`=? WHERE `user_id`=?';
			dbConnection.Query(res, sql, [newPassEncrypt, '', 0, userID], function (response) {
				var data = {"message": "Password changed succesfully"};
				sendResponse.sendSuccessData(data, res);
			});
		});
};

function getUserIdFromEmail (res, email, callback) {

	var sql = 'SELECT `user_id` FROM `user_info` WHERE `email`=? LIMIT 1';
  dbConnection.Query(res, sql, [email], function (response) {

    if (response.length == 0) {
      var error = 'Email not registered';
      sendResponse.sendErrorMessage(error, res);
    } else {
      var userID = response[0].user_id;
      callback(null, userID);
    }
  });
}

function checkToken (res, link, callback) {
	var pos = link.indexOf('token=') + 6;			//get index of token in link
	var token = link.slice(pos);

	var sql = 'SELECT `user_id` FROM `user_info` WHERE `one_time_password_token`=? LIMIT 1';
	dbConnection.Query(res, sql, [token], function (response) {

		if (response.length == 0) {
			var error = 'Invalid reset password token';
			sendResponse.sendErrorMessage(error, res);
		} else {
			var userID = response[0].user_id;
			callback(null, userID);
		}
	});
}

function checkPassAndConfirmPass (res, newPass, confirmNewPass, callback) {

	if (newPass !== confirmNewPass) {
		var error = 'Passwords do not match';
		sendResponse.sendErrorMessage(error, res);
	} else {
		callback(null);
	}
}